// Bundle FontAwesome styles instead of inserting them at runtime
// This is necessary for Content Security Policy to work properly

import "@fortawesome/fontawesome-svg-core/styles.css";
import { config } from "@fortawesome/fontawesome-svg-core";

config.autoAddCss = false;
