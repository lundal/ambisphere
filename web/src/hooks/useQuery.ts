import { useLocation } from "react-router-dom";
import { parseQuery, Query } from "../utils/QueryUtils";

export function useQuery(): Query {
  const location = useLocation();
  return parseQuery(location.search);
}
