import {
  Ambiance,
  Sound,
  SoundGroup,
  Visual,
  VisualGroup,
} from "../../api/models";

export type Dialog =
  | None
  | CreateSoundGroup
  | EditSoundGroup
  | DeleteSoundGroup
  | CreateSound
  | EditSound
  | DeleteSound
  | CreateVisualGroup
  | EditVisualGroup
  | DeleteVisualGroup
  | CreateVisual
  | EditVisual
  | DeleteVisual
  | EditAmbiance
  | DeleteAmbiance;

type None = {
  type: "None";
};

type CreateSoundGroup = {
  type: "CreateSoundGroup";
  ambianceId: string;
};

type EditSoundGroup = {
  type: "EditSoundGroup";
  ambianceId: string;
  group: SoundGroup;
};

type DeleteSoundGroup = {
  type: "DeleteSoundGroup";
  ambianceId: string;
  group: SoundGroup;
};

type CreateSound = {
  type: "CreateSound";
  ambianceId: string;
  group: SoundGroup;
};

type EditSound = {
  type: "EditSound";
  ambianceId: string;
  group: SoundGroup;
  sound: Sound;
};

type DeleteSound = {
  type: "DeleteSound";
  ambianceId: string;
  groupId: string;
  sound: Sound;
};

type CreateVisualGroup = {
  type: "CreateVisualGroup";
  ambianceId: string;
};

type EditVisualGroup = {
  type: "EditVisualGroup";
  ambianceId: string;
  group: VisualGroup;
};

type DeleteVisualGroup = {
  type: "DeleteVisualGroup";
  ambianceId: string;
  group: VisualGroup;
};

type CreateVisual = {
  type: "CreateVisual";
  ambianceId: string;
  group: VisualGroup;
};

type EditVisual = {
  type: "EditVisual";
  ambianceId: string;
  group: VisualGroup;
  visual: Visual;
};

type DeleteVisual = {
  type: "DeleteVisual";
  ambianceId: string;
  groupId: string;
  visual: Visual;
};

type EditAmbiance = {
  type: "EditAmbiance";
  ambiance: Ambiance;
};

type DeleteAmbiance = {
  type: "DeleteAmbiance";
  ambianceId: string;
};
