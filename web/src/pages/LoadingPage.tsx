import * as React from "react";
import { LoadingLayout } from "@lundal/nyx-react";

export function LoadingPage(): JSX.Element {
  return <LoadingLayout />;
}
