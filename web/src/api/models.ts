export type Error = {
  code: number;
  reason: string;
  details: string | null;
};

export type Config = {
  user: User | null;
  accountUrl: string;
  appLinks: AppLink[];
};

export type User = {
  id: string;
  name: string;
};

export type AppLink = {
  name: string;
  iconUrl: string;
  linkUrl: string;
};

export type Usage = {
  current: number;
  limit: number;
};

export type CreateFile = {
  mediatype: string;
  data: string;
};

export type File = {
  mediatype: string;
  url: string;
};

export type CreateAmbiance = {
  name: string;
};

export type CreateSoundGroup = {
  name: string;
  crossfadeMs: number;
  shuffle: boolean;
};

export type CreateSound = {
  name: string;
  cropStartMs: number;
  cropEndMs: number;
  lengthMs: number;
  volumePercent: number;
  audioUrl: string;
};

export type CreateVisualGroup = {
  name: string;
  backgroundColor: string;
  containImage: boolean;
  crossfadeMs: number;
};

export type CreateVisual = {
  name: string;
  thumbnailUrl: string;
  imageUrl: string;
};

export type AmbianceSummary = {
  id: string;
  name: string;
  sounds: number;
  visuals: number;
};

export type Ambiance = {
  id: string;
  name: string;
  soundGroups: SoundGroup[];
  visualGroups: VisualGroup[];
};

export type SoundGroup = {
  id: string;
  name: string;
  crossfadeMs: number;
  shuffle: boolean;
  sounds: Sound[];
};

export type Sound = {
  id: string;
  name: string;
  cropStartMs: number;
  cropEndMs: number;
  lengthMs: number;
  volumePercent: number;
  audioUrl: string;
};

export type VisualGroup = {
  id: string;
  name: string;
  backgroundColor: string;
  containImage: boolean;
  crossfadeMs: number;
  visuals: Visual[];
};

export type Visual = {
  id: string;
  name: string;
  thumbnailUrl: string;
  imageUrl: string;
};

export type Move = {
  groupId: string;
  position: number;
};

export type Actives = {
  soundGroups: Active[];
  visual: Active | null;
};

export type Active = {
  id: string;
  startedMs: number;
};
