import { get } from "./request";
import { Config } from "./models";

export function getConfig(): Promise<Config> {
  return get("/api/v1/config");
}
