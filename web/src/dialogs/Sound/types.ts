export type Fields = {
  name: string;
  cropStartMs: string;
  cropEndMs: string;
  lengthMs: number;
  volumePercent: string;
  audioUrl: string;
};

export type Errors = {
  name?: string;
  cropStartMs?: string;
  cropEndMs?: string;
  volumePercent?: string;
  audioUrl?: string;
};
