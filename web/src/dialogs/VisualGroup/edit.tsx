import * as React from "react";
import {
  Button,
  Column,
  Dialog,
  Heading,
  Input,
  Message,
  RadioGroup,
  Row,
  TextBox,
} from "@lundal/nyx-react";
import { containImageOptions, Errors, Fields } from "./types";
import { Form } from "../../components/Form";
import { updateVisualGroup } from "../../api/ambianceApi";
import { Error, VisualGroup } from "../../api/models";
import { useForm } from "../../hooks/useForm";
import {
  validateColor,
  validateCrossfade,
  validateName,
} from "../../common/Validators";

type Props = {
  ambianceId: string;
  group: VisualGroup;
  onUpdated: () => void;
  onClose: () => void;
};

export function EditVisualGroupDialog(props: Props): JSX.Element {
  const form = useForm<Fields, Errors, VisualGroup, Error>(
    {
      name: props.group.name,
      backgroundColor: props.group.backgroundColor,
      containImage: props.group.containImage,
      crossfadeMs: props.group.crossfadeMs.toString(),
    },
    (fields) => ({
      name: validateName(fields.name),
      backgroundColor: validateColor(fields.backgroundColor),
      crossfadeMs: validateCrossfade(fields.crossfadeMs),
    }),
    (fields) =>
      updateVisualGroup(props.ambianceId, props.group.id, {
        name: fields.name,
        backgroundColor: fields.backgroundColor,
        containImage: fields.containImage,
        crossfadeMs: parseInt(fields.crossfadeMs),
      }),
    () => {
      props.onUpdated();
      props.onClose();
    }
  );

  return (
    <Dialog onDismiss={() => props.onClose()}>
      <Form onSubmit={form.submit}>
        <Column>
          <Heading level={2}>Edit Visual Group</Heading>
          <Input
            id="name"
            label="Name"
            help="A name for the visual group"
            error={form.errors.name}
            input={
              <TextBox
                value={form.fields.name}
                onChange={(value) => form.update({ name: value })}
              />
            }
          />
          <Input
            id="color"
            label="Background color"
            help="The background for visuals with transparent areas"
            error={form.errors.backgroundColor}
            input={
              <TextBox
                value={form.fields.backgroundColor}
                onChange={(value) => form.update({ backgroundColor: value })}
              />
            }
          />
          <Input
            id="crossfade"
            label="Crossfade"
            help="The duration for visuals to fade in and out (in milliseconds)"
            error={form.errors.crossfadeMs}
            input={
              <TextBox
                value={form.fields.crossfadeMs}
                onChange={(value) => form.update({ crossfadeMs: value })}
              />
            }
          />
          <Input
            id="contain"
            label="Image size"
            input={
              <RadioGroup
                options={containImageOptions}
                value={form.fields.containImage}
                onChange={(value) => form.update({ containImage: value })}
              />
            }
          />
          <div />
          <Row>
            <Button
              color={form.valid ? "white" : undefined}
              label="Save visual group"
            />
            <Button label="Cancel" onClick={() => props.onClose()} />
          </Row>
          {form.state.type == "Failure" && (
            <Message type="error" text={form.state.error.reason} />
          )}
        </Column>
      </Form>
    </Dialog>
  );
}
