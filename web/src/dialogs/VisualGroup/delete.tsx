import * as React from "react";
import {
  Button,
  Column,
  Dialog,
  Heading,
  Message,
  Row,
} from "@lundal/nyx-react";
import { Form } from "../../components/Form";
import { deleteVisualGroup } from "../../api/ambianceApi";
import { Error, VisualGroup } from "../../api/models";
import { useForm } from "../../hooks/useForm";

type Props = {
  ambianceId: string;
  group: VisualGroup;
  onDeleted: () => void;
  onClose: () => void;
};

export function DeleteVisualGroupDialog(props: Props): JSX.Element {
  const form = useForm<{}, {}, void, Error>(
    {},
    () => ({}),
    () => deleteVisualGroup(props.ambianceId, props.group.id),
    () => {
      props.onDeleted();
      props.onClose();
    }
  );

  return (
    <Dialog onDismiss={() => props.onClose()}>
      <Form onSubmit={form.submit}>
        <Column>
          <Heading level={2}>Delete Visual Group</Heading>
          <p>Are you sure you want to delete "{props.group.name}"?</p>
          <div />
          <Row>
            <Button color="red" label="Delete visual group" />
            <Button label="Cancel" onClick={() => props.onClose()} />
          </Row>
          {form.state.type == "Failure" && (
            <Message type="error" text={form.state.error.reason} />
          )}
        </Column>
      </Form>
    </Dialog>
  );
}
