export type Fields = {
  name: string;
  thumbnailUrl: string;
  imageUrl: string;
};

export type Errors = {
  name?: string;
};
