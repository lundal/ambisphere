import * as React from "react";
import {
  Button,
  Column,
  Dialog,
  Heading,
  Message,
  Row,
} from "@lundal/nyx-react";
import { Form } from "../../components/Form";
import { deleteVisual } from "../../api/ambianceApi";
import { Error, Visual } from "../../api/models";
import { useForm } from "../../hooks/useForm";

type Props = {
  ambianceId: string;
  groupId: string;
  visual: Visual;
  onDeleted: () => void;
  onClose: () => void;
};

export function DeleteVisualDialog(props: Props): JSX.Element {
  const form = useForm<{}, {}, void, Error>(
    {},
    () => ({}),
    () => deleteVisual(props.ambianceId, props.groupId, props.visual.id),
    () => {
      props.onDeleted();
      props.onClose();
    }
  );

  return (
    <Dialog onDismiss={() => props.onClose()}>
      <Form onSubmit={form.submit}>
        <Column>
          <Heading level={2}>Delete Visual</Heading>
          <p>Are you sure you want to delete "{props.visual.name}"?</p>
          <div />
          <Row>
            <Button color="red" label="Delete visual" />
            <Button label="Cancel" onClick={() => props.onClose()} />
          </Row>
          {form.state.type == "Failure" && (
            <Message type="error" text={form.state.error.reason} />
          )}
        </Column>
      </Form>
    </Dialog>
  );
}
