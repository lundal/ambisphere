export type Fields = {
  name: string;
  crossfadeMs: string;
  shuffle: boolean;
};

export type Errors = {
  name?: string;
  crossfadeMs?: string;
};
