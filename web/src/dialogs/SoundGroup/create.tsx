import * as React from "react";
import {
  Button,
  CheckBox,
  Column,
  Dialog,
  Heading,
  Input,
  Message,
  Row,
  TextBox,
} from "@lundal/nyx-react";
import { Errors, Fields } from "./types";
import { Form } from "../../components/Form";
import { createSoundGroup } from "../../api/ambianceApi";
import { Error, SoundGroup } from "../../api/models";
import { useForm } from "../../hooks/useForm";
import { validateCrossfade, validateName } from "../../common/Validators";

type Props = {
  ambianceId: string;
  onCreated: () => void;
  onClose: () => void;
};

export function CreateSoundGroupDialog(props: Props): JSX.Element {
  const form = useForm<Fields, Errors, SoundGroup, Error>(
    {
      name: "",
      crossfadeMs: "1000",
      shuffle: true,
    },
    (fields) => ({
      name: validateName(fields.name),
      crossfadeMs: validateCrossfade(fields.crossfadeMs),
    }),
    (fields) =>
      createSoundGroup(props.ambianceId, {
        name: fields.name,
        crossfadeMs: parseInt(fields.crossfadeMs),
        shuffle: fields.shuffle,
      }),
    () => {
      props.onCreated();
      props.onClose();
    }
  );

  return (
    <Dialog onDismiss={() => props.onClose()}>
      <Form onSubmit={form.submit}>
        <Column>
          <Heading level={2}>Create Sound Group</Heading>
          <Input
            id="name"
            label="Name"
            help="A name for the sound group"
            error={form.errors.name}
            input={
              <TextBox
                value={form.fields.name}
                onChange={(value) => form.update({ name: value })}
              />
            }
          />
          <Input
            id="crossfade"
            label="Crossfade"
            help="The duration for sounds to fade in and out (in milliseconds)"
            error={form.errors.crossfadeMs}
            input={
              <TextBox
                value={form.fields.crossfadeMs}
                onChange={(value) => form.update({ crossfadeMs: value })}
              />
            }
          />
          <CheckBox
            id="shuffle"
            label="Shuffle"
            value={form.fields.shuffle}
            onChange={(value) => form.update({ shuffle: value })}
          />
          <div />
          <Row>
            <Button
              color={form.valid ? "white" : undefined}
              label="Create sound group"
            />
            <Button label="Cancel" onClick={() => props.onClose()} />
          </Row>
          {form.state.type == "Failure" && (
            <Message type="error" text={form.state.error.reason} />
          )}
        </Column>
      </Form>
    </Dialog>
  );
}
