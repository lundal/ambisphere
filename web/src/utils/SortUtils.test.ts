import { describe, it } from "mocha";
import { expect } from "chai";
import { byShuffle } from "./SortUtils";

describe("byShuffle", () => {
  it("seed 0", () => {
    const original = [{ id: "a" }, { id: "b" }];
    const sorted = original.slice().sort(byShuffle(0));
    expect(sorted[0]).to.equal(original[1]);
    expect(sorted[1]).to.equal(original[0]);
  });
  it("seed 1", () => {
    const original = [{ id: "a" }, { id: "b" }];
    const sorted = original.slice().sort(byShuffle(1));
    expect(sorted[0]).to.equal(original[0]);
    expect(sorted[1]).to.equal(original[1]);
  });
  it("seed 2", () => {
    const original = [{ id: "a" }, { id: "b" }];
    const sorted = original.slice().sort(byShuffle(2));
    expect(sorted[0]).to.equal(original[1]);
    expect(sorted[1]).to.equal(original[0]);
  });
  it("seed 3", () => {
    const original = [{ id: "a" }, { id: "b" }];
    const sorted = original.slice().sort(byShuffle(3));
    expect(sorted[0]).to.equal(original[0]);
    expect(sorted[1]).to.equal(original[1]);
  });
  it("seed 4", () => {
    const original = [{ id: "a" }, { id: "b" }];
    const sorted = original.slice().sort(byShuffle(4));
    expect(sorted[0]).to.equal(original[0]);
    expect(sorted[1]).to.equal(original[1]);
  });
});
