export type Query = {
  get: (key: string) => string | null;
  contains: (key: string, value: string | null) => boolean;
  with: (key: string, value: string) => Query;
  without: (key: string) => Query;
  toString: () => string;
};

export function parseQuery(search?: string): Query {
  return {
    get: (key) => {
      const params = new URLSearchParams(search);
      return params.get(key);
    },
    contains: (key, value) => {
      const params = new URLSearchParams(search);
      return params.get(key) === value;
    },
    with: (key, value) => {
      const params = new URLSearchParams(search);
      params.set(key, value);
      return parseQuery(params.toString());
    },
    without: (key) => {
      const params = new URLSearchParams(search);
      params.delete(key);
      return parseQuery(params.toString());
    },
    toString: () => {
      const params = new URLSearchParams(search);
      const s = params.toString();
      return s === "" ? "" : "?" + s;
    },
  };
}
