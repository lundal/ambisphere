import { describe, it } from "mocha";
import { expect } from "chai";
import { parseQuery } from "./QueryUtils";

describe("parseQuery", () => {
  it("empty", () => {
    const query = parseQuery();
    expect(query.toString()).to.equal("");
  });
  it("empty", () => {
    const query = parseQuery("");
    expect(query.toString()).to.equal("");
  });
  it("question mark", () => {
    const query = parseQuery("?");
    expect(query.toString()).to.equal("");
  });
  it("not empty", () => {
    const query = parseQuery("?abc=123");
    expect(query.toString()).to.equal("?abc=123");
  });
  it("with", () => {
    const query = parseQuery("").with("abc", "123");
    expect(query.toString()).to.equal("?abc=123");
  });
  it("without", () => {
    const query = parseQuery("?abc=123").without("abc");
    expect(query.toString()).to.equal("");
  });
  it("contains", () => {
    const query = parseQuery("?abc=123");
    expect(query.contains("abc", "123")).to.be.true;
    expect(query.contains("xyz", "123")).to.be.false;
  });
  it("get", () => {
    const query = parseQuery("?abc=123");
    expect(query.get("abc")).to.equal("123");
    expect(query.get("xyz")).to.be.null;
  });
});
