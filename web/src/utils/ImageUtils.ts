import { toFile } from "./FileUtils";

export function scaleImage(
  image: ImageBitmap,
  maxArea: number,
  quality: number
) {
  const { width, height } = calcMaxSize(image, maxArea);

  const canvas = document.createElement("canvas");
  canvas.width = width;
  canvas.height = height;

  const context = canvas.getContext("2d")!;
  context.imageSmoothingEnabled = true;
  context.imageSmoothingQuality = "high";
  context.drawImage(image, 0, 0, width, height);

  const dataUrl = canvas.toDataURL("image/webp", quality);
  return toFile(dataUrl);
}

function calcMaxSize(image: ImageBitmap, maxArea: number) {
  const area = image.width * image.height;
  const scale = area < maxArea ? 1 : Math.sqrt(maxArea / area);
  return {
    width: Math.floor(image.width * scale),
    height: Math.floor(image.height * scale),
  };
}
