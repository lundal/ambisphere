#!/bin/bash
set -e

systemctl --user enable podman.socket
systemctl --user start podman.socket

export DOCKER_HOST="unix:///run/user/$UID/podman/podman.sock"

if ! grep --quiet DOCKER_HOST ~/.bashrc
then echo "export DOCKER_HOST=unix:///run/user/$UID/podman/podman.sock" >> ~/.bashrc
fi

echo "You can now use docker-compose with rootless podman"
