import type { PlaywrightTestConfig } from "@playwright/test";

const config: PlaywrightTestConfig = {
  testDir: "./src",
  outputDir: "./output",
  reporter: "list",
  use: {
    browserName: "chromium",
    headless: !!process.env.HEADLESS,
    video: "off",
  },
};

export default config;
