import { expect, test } from "@playwright/test";
import { gotoAmbisphere, signIn } from "./utils";

test.beforeEach(async ({ page }) => {
  await signIn(page);
});

test.describe("Home", () => {
  test.beforeEach(async ({ page }) => {
    await gotoAmbisphere(page, "/");
  });

  test("should display ambiances", async ({ page }) => {
    await expect(page.locator("main")).toContainText("Select Ambiance");
  });
});
