import { Page } from "@playwright/test";

export async function gotoAmbisphere(page: Page, path: string): Promise<void> {
  const ambisphereUrl = process.env.AMBISPHERE_URL || "http://localhost:3201";
  await page.goto(ambisphereUrl + "/#" + path);
}

export async function gotoAccount(page: Page, path: string): Promise<void> {
  const accountUrl = process.env.ACCOUNT_URL || "http://localhost:3200";
  await page.goto(accountUrl + path);
}

export async function signIn(
  page: Page,
  username: string = "normaluser",
  password: string = "password1234"
): Promise<void> {
  await gotoAccount(page, "/sign-in");
  await page.locator("id=username").fill(username);
  await page.locator("id=password").fill(password);
  await page.locator("button >> text='Sign in'").click();
  await page.waitForNavigation();
}
