FROM ubuntu:latest

# Install app
WORKDIR /app
COPY server/build/application /app/application

EXPOSE 3201

CMD ["./application"]
