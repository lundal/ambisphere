package io.lundal.ambisphere.util

import io.lundal.ambisphere.util.IdUtils.randomId
import org.junit.jupiter.api.Test

internal class IdUtilsTest {

    @Test
    fun test_randomId() {
        val id = randomId()
        println(id)
    }
}
