create table files
(
    id        text   not null primary key,
    user_id   text   not null,
    mediatype text   not null,
    size      int    not null,
    data      bytea,
    created   bigint not null
);
