create table active_sound_groups
(
    ambiance_id    text   not null references ambiances (id) on delete cascade,
    sound_group_id text   not null references sound_groups (id) on delete cascade,
    created        bigint not null,
    unique (ambiance_id, sound_group_id)
);

create table active_visuals
(
    ambiance_id text   not null references ambiances (id) on delete cascade,
    visual_id   text   not null references visuals (id) on delete cascade,
    created     bigint not null,
    unique (ambiance_id, visual_id)
);
