create table ambiances
(
    id      text   not null primary key,
    user_id text   not null,
    name    text   not null,
    created bigint not null
);

create table sound_groups
(
    id           text    not null primary key,
    ambiance_id  text    not null references ambiances (id) on delete cascade,
    name         text    not null,
    crossfade_ms int     not null,
    shuffle      boolean not null
);

create table sounds
(
    id             text not null primary key,
    group_id       text not null references sound_groups (id) on delete cascade,
    name           text not null,
    crop_start_ms  int  not null,
    crop_end_ms    int  not null,
    length_ms      int  not null,
    volume_percent int  not null,
    audio_url      text not null
);

create table visual_groups
(
    id               text    not null primary key,
    ambiance_id      text    not null references ambiances (id) on delete cascade,
    name             text    not null,
    background_color text    not null,
    contain_image    boolean not null,
    crossfade_ms     int     not null
);

create table visuals
(
    id            text not null primary key,
    group_id      text not null references visual_groups (id) on delete cascade,
    name          text not null,
    thumbnail_url text not null,
    image_url     text not null
);
