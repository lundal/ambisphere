package io.lundal.mediaconverter.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
enum class ImageFormat(val type: String) {
    JPEG("image/jpeg"),
    PNG("image/png"),
    WEBP("image/webp"),
    AVIF("image/avif")
}
