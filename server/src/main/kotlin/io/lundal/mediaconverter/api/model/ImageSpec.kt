package io.lundal.mediaconverter.api.model

import io.quarkus.runtime.annotations.RegisterForReflection
import javax.validation.Valid
import javax.validation.constraints.Max
import javax.validation.constraints.Min

@RegisterForReflection
data class ImageSpec(
    @field:Valid
    val format: ImageFormat,

    @field:Valid
    val size: ImageSize,

    @field:Min(0)
    @field:Max(100)
    val quality: Int,
)
