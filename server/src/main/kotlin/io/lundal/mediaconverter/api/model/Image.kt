package io.lundal.mediaconverter.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class Image(
    val format: ImageFormat,
    val data: ByteArray
)
