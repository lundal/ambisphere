package io.lundal.mediaconverter.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class Audio(
    val format: AudioFormat,
    val data: ByteArray
)
