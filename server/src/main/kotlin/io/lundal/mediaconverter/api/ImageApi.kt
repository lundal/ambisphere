package io.lundal.mediaconverter.api

import io.lundal.ambisphere.auth.AuthenticationClientHeaderFactory
import io.lundal.mediaconverter.api.model.ConvertImageRequest
import io.lundal.mediaconverter.api.model.Image
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import javax.ws.rs.Consumes
import javax.ws.rs.POST
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType.APPLICATION_JSON

@RegisterClientHeaders(AuthenticationClientHeaderFactory::class)
@RegisterRestClient(configKey = "mediaconverter")
@Path("/api/v1/images")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
interface ImageApi {

    @POST
    @Path("/convert")
    fun convertImage(request: ConvertImageRequest): List<Image>

}
