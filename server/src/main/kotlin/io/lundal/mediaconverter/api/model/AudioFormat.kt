package io.lundal.mediaconverter.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
enum class AudioFormat(val type: String) {
    MP3("audio/mpeg"),
    WAV("audio/wav"),
    OGG("audio/ogg"),
    OPUS("audio/opus")
}
