package io.lundal.account.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class IdentifyAccount(
    val sessionCookie: String,
    val dummy: String? = null,
)
