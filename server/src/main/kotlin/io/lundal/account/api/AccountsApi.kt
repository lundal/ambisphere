package io.lundal.account.api

import io.lundal.account.api.model.IdentifyAccount
import io.lundal.account.api.model.Account
import io.lundal.ambisphere.auth.AuthenticationClientHeaderFactory
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient
import javax.ws.rs.*
import javax.ws.rs.core.MediaType.APPLICATION_JSON

@RegisterClientHeaders(AuthenticationClientHeaderFactory::class)
@RegisterRestClient(configKey = "account")
@Path("/api/v1/accounts")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
interface AccountsApi {

    @GET
    fun listAccounts(): List<Account>

    @POST
    @Path("/identify")
    fun identifyAccount(identify: IdentifyAccount): Account

}
