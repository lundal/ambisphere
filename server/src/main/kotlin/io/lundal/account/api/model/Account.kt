package io.lundal.account.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class Account(
    val id: String,
    val username: String,
    val type: String,
)
