package io.lundal.ambisphere.auth

sealed class Authentication {
    data class User(val id: String, val name: String) : Authentication()
}
