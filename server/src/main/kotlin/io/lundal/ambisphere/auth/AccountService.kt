package io.lundal.ambisphere.auth

import io.lundal.account.api.AccountsApi
import io.lundal.account.api.model.IdentifyAccount
import io.lundal.account.api.model.Account
import io.quarkus.cache.CacheResult
import org.eclipse.microprofile.rest.client.inject.RestClient
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.WebApplicationException

@ApplicationScoped
class AccountService(
    @RestClient private val accountsApi: AccountsApi,
) {

    @CacheResult(cacheName = "sessions")
    fun identifyAccount(sessionCookie: String): Account? {
        try {
            return accountsApi.identifyAccount(IdentifyAccount(sessionCookie))
        } catch (e: WebApplicationException) {
            if (e.response.status == 401) {
                return null
            }
            throw e
        }
    }

}
