package io.lundal.ambisphere.auth

import io.lundal.ambisphere.exceptions.ServiceExceptions
import io.vertx.ext.web.RoutingContext
import javax.enterprise.context.RequestScoped

@RequestScoped
class AuthenticationService(
    private val routingContext: RoutingContext,
    private val accountService: AccountService,
) {

    fun requireUser(condition: (Authentication.User) -> Boolean): Authentication.User {
        return requireUser().also { if (!condition(it)) throw ServiceExceptions.permissionDenied() }
    }

    fun requireUser(): Authentication.User {
        val sessionCookie = routingContext.request().getCookie("session")?.value ?: throw ServiceExceptions.sessionCookieMissing()
        val account = accountService.identifyAccount(sessionCookie) ?: throw ServiceExceptions.sessionCookieInvalid()
        return Authentication.User(account.id, account.username)
    }

}
