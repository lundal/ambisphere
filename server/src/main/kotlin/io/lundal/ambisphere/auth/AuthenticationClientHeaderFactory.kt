package io.lundal.ambisphere.auth

import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.ext.ClientHeadersFactory
import javax.enterprise.context.ApplicationScoped
import javax.ws.rs.core.MultivaluedHashMap
import javax.ws.rs.core.MultivaluedMap

@ApplicationScoped
class AuthenticationClientHeaderFactory(
    @ConfigProperty(name = "api-key") private val apiKey: String,
) : ClientHeadersFactory {

    override fun update(
        incomingHeaders: MultivaluedMap<String, String>,
        clientOutgoingHeaders: MultivaluedMap<String, String>
    ): MultivaluedMap<String, String> {
        if (clientOutgoingHeaders.getFirst("Client-Header-Factory") == "Disabled") {
            return MultivaluedHashMap()
        }
        val headers = MultivaluedHashMap<String, String>()
        headers.add("authorization", "api-key $apiKey")
        return headers
    }

}
