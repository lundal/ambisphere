package io.lundal.ambisphere.util

import java.sql.ResultSet
import javax.sql.DataSource

object DatabaseUtils {

    fun DataSource.update(sql: String, vararg params: Any?): Int {
        return this.connection.use { connection ->
            connection.prepareStatement(sql).use { statement ->
                params.forEachIndexed { index, param ->
                    statement.setObject(index + 1, param)
                }
                statement.executeUpdate()
            }
        }
    }

    fun <T> DataSource.query(sql: String, vararg params: Any?, rowMapper: (rs: ResultSet) -> T): List<T> {
        return this.connection.use { connection ->
            connection.prepareStatement(sql).use { statement ->
                params.forEachIndexed { index, param ->
                    statement.setObject(index + 1, param)
                }
                statement.executeQuery().use { resultSet ->
                    resultSet.mapRows(rowMapper)
                }
            }
        }
    }

    fun <T> DataSource.queryFirst(sql: String, vararg params: Any?, rowMapper: (rs: ResultSet) -> T): T? {
        return this.connection.use { connection ->
            connection.prepareStatement(sql).use { statement ->
                params.forEachIndexed { index, param ->
                    statement.setObject(index + 1, param)
                }
                statement.executeQuery().use { resultSet ->
                    resultSet.mapRow(rowMapper)
                }
            }
        }
    }

    fun DataSource.queryForString(sql: String, vararg params: Any?): String? {
        return this.queryFirst(sql, *params) { rs -> rs.getString(1) }
    }

    fun DataSource.queryForInt(sql: String, vararg params: Any?): Int? {
        return this.queryFirst(sql, *params) { rs -> rs.getInt(1) }
    }

    fun DataSource.queryForLong(sql: String, vararg params: Any?): Long? {
        return this.queryFirst(sql, *params) { rs -> rs.getLong(1) }
    }

    fun DataSource.queryForBoolean(sql: String, vararg params: Any?): Boolean? {
        return this.queryFirst(sql, *params) { rs -> rs.getBoolean(1) }
    }

    private fun <T> ResultSet.mapRows(mapper: (rs: ResultSet) -> T): List<T> {
        val results = mutableListOf<T>()
        while (this.next()) {
            results.add(mapper(this))
        }
        return results
    }

    private fun <T> ResultSet.mapRow(mapper: (rs: ResultSet) -> T): T? {
        if (this.next()) {
            return mapper(this)
        }
        return null
    }

}
