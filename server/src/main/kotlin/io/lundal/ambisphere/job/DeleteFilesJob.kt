package io.lundal.ambisphere.job

import io.lundal.ambisphere.db.FileDao
import io.lundal.ambisphere.s3.S3Service
import io.quarkus.scheduler.Scheduled
import io.quarkus.scheduler.Scheduled.ConcurrentExecution.SKIP
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Instant
import java.time.temporal.ChronoUnit.MINUTES
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class DeleteFilesJob(
    private val s3Service: S3Service,
    private val fileDao: FileDao,
) {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Scheduled(every = "1m", delayed = "1m", concurrentExecution = SKIP)
    fun deleteFilesJob() {
        val createdBefore = Instant.now().minus(10, MINUTES).toEpochMilli()
        val limit = 10

        fileDao.findUnused(createdBefore, limit)
            .forEach { fileRow ->
                logger.info("Deleting unused file with id '${fileRow.id}'")
                if (fileRow.data == null) {
                    s3Service.deleteFile(fileRow.id)
                }
                fileDao.delete(fileRow.id)
            }
    }

}
