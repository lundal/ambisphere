package io.lundal.ambisphere.job

import io.lundal.account.api.AccountsApi
import io.lundal.ambisphere.db.AmbianceDao
import io.quarkus.scheduler.Scheduled
import io.quarkus.scheduler.Scheduled.ConcurrentExecution.SKIP
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class DeleteAmbiancesJob(
    @RestClient private val accountsApi: AccountsApi,
    private val ambianceDao: AmbianceDao
) {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Scheduled(every = "10m", delayed = "5m", concurrentExecution = SKIP)
    fun deleteAmbiancesJob() {
        val userIds = ambianceDao.uniqueUserIds()
        val userIdsToKeep = accountsApi.listAccounts().map { it.id }.toSet()

        userIds
            .filter { userId -> userId !in userIdsToKeep }
            .forEach { userId ->
                logger.info("Deleting all ambiances for user with id '$userId'")
                ambianceDao.deleteForUser(userId)
            }
    }

}
