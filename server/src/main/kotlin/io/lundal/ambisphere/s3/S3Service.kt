package io.lundal.ambisphere.s3

import org.eclipse.microprofile.config.inject.ConfigProperty
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest
import software.amazon.awssdk.services.s3.model.ObjectCannedACL
import software.amazon.awssdk.services.s3.model.PutObjectRequest
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class S3Service(
    @ConfigProperty(name = "s3.bucket") private val bucket: String,
    @ConfigProperty(name = "s3.folder") private val folder: String,
    private val s3: S3Client,
) {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    fun uploadFile(id: String, mediatype: String, data: ByteArray) {
        s3.putObject(
            PutObjectRequest.builder()
                .bucket(bucket)
                .acl(ObjectCannedACL.PUBLIC_READ)
                .key("$folder/$id")
                .contentType(mediatype)
                .build(),
            RequestBody.fromBytes(data)
        )

        logger.info("Stored $id in S3")
    }

    fun deleteFile(id: String) {
        s3.deleteObject(
            DeleteObjectRequest.builder()
                .bucket(bucket)
                .key("$folder/$id")
                .build()
        )

        logger.info("Deleted $id from S3")
    }

}
