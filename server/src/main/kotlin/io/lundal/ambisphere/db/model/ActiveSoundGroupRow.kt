package io.lundal.ambisphere.db.model

import java.time.Instant

data class ActiveSoundGroupRow(
    val ambianceId: String,
    val soundGroupId: String,
    val created: Long = Instant.now().toEpochMilli(),
)
