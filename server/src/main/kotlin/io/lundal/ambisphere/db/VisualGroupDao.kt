package io.lundal.ambisphere.db

import io.lundal.ambisphere.db.model.VisualGroupRow
import io.lundal.ambisphere.util.DatabaseUtils.query
import io.lundal.ambisphere.util.DatabaseUtils.queryFirst
import io.lundal.ambisphere.util.DatabaseUtils.update
import java.sql.ResultSet
import javax.enterprise.context.ApplicationScoped
import javax.sql.DataSource

@ApplicationScoped
class VisualGroupDao(private val ds: DataSource) {

    fun insert(row: VisualGroupRow) {
        val sql = """
            insert into visual_groups (id, ambiance_id, name, background_color, contain_image, crossfade_ms)
            values (?, ?, ?, ?, ?, ?)
            """
        ds.update(sql, row.id, row.ambianceId, row.name, row.backgroundColor, row.containImage, row.crossfadeMs)
    }

    fun update(row: VisualGroupRow) {
        val sql = """
            update visual_groups
            set name = ?, background_color = ?, contain_image = ?, crossfade_ms = ?
            where id = ?
            """
        ds.update(sql, row.name, row.backgroundColor, row.containImage, row.crossfadeMs, row.id)
    }

    fun listForAmbiance(ambianceId: String): List<VisualGroupRow> {
        val sql = "select * from visual_groups where ambiance_id = ?"
        return ds.query(sql, ambianceId, rowMapper = ::mapRow)
    }

    fun get(id: String): VisualGroupRow? {
        val sql = "select * from visual_groups where id = ?"
        return ds.queryFirst(sql, id, rowMapper = ::mapRow)
    }

    fun delete(id: String) {
        val sql = "delete from visual_groups where id = ?"
        ds.update(sql, id)
    }

    private fun mapRow(rs: ResultSet) = VisualGroupRow(
        id = rs.getString("id"),
        ambianceId = rs.getString("ambiance_id"),
        name = rs.getString("name"),
        backgroundColor = rs.getString("background_color"),
        containImage = rs.getBoolean("contain_image"),
        crossfadeMs = rs.getInt("crossfade_ms")
    )
}
