package io.lundal.ambisphere.db

import io.lundal.ambisphere.db.model.FileRow
import io.lundal.ambisphere.util.DatabaseUtils.query
import io.lundal.ambisphere.util.DatabaseUtils.queryFirst
import io.lundal.ambisphere.util.DatabaseUtils.queryForInt
import io.lundal.ambisphere.util.DatabaseUtils.update
import java.sql.ResultSet
import javax.enterprise.context.ApplicationScoped
import javax.sql.DataSource

@ApplicationScoped
class FileDao(private val ds: DataSource) {

    fun insert(row: FileRow) {
        val sql = """
            insert into files (id, user_id, mediatype, size, data, created)
            values (?, ?, ?, ?, ?, ?)
            """
        ds.update(sql, row.id, row.userId, row.mediatype, row.size, row.data, row.created)
    }

    fun get(id: String): FileRow? {
        val sql = "select * from files where id = ?"
        return ds.queryFirst(sql, id, rowMapper = ::mapRow)
    }

    fun delete(id: String) {
        val sql = "delete from files where id = ?"
        ds.update(sql, id)
    }

    fun getUsage(userId: String): Int {
        val sql = "select sum(size) from files where user_id = ?"
        return ds.queryForInt(sql, userId) ?: 0
    }

    fun findUnused(createdBefore: Long, limit: Int): List<FileRow> {
        val sql = """
            select * from files
            where created < ?
            and not exists(
                select 1 from sounds
                where audio_url like '%/' || files.id
            )
            and not exists(
                select 1 from visuals
                where thumbnail_url like '%/' || files.id
                   or image_url like '%/' || files.id
            )
            limit ?
            """
        return ds.query(sql, createdBefore, limit, rowMapper = ::mapRow)
    }

    private fun mapRow(rs: ResultSet) = FileRow(
        id = rs.getString("id"),
        userId = rs.getString("user_id"),
        mediatype = rs.getString("mediatype"),
        size = rs.getInt("size"),
        data = rs.getBytes("data"),
        created = rs.getLong("created")
    )
}
