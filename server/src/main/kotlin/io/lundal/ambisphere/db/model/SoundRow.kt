package io.lundal.ambisphere.db.model

import io.lundal.ambisphere.util.IdUtils

data class SoundRow(
    val id: String = IdUtils.randomId(),
    val groupId: String,
    val name: String,
    val cropStartMs: Int,
    val cropEndMs: Int,
    val lengthMs: Int,
    val volumePercent: Int,
    val audioUrl: String
)
