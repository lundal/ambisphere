package io.lundal.ambisphere.db.model

import io.lundal.ambisphere.util.IdUtils

data class SoundGroupRow(
    val id: String = IdUtils.randomId(),
    val ambianceId: String,
    val name: String,
    val crossfadeMs: Int,
    val shuffle: Boolean
)
