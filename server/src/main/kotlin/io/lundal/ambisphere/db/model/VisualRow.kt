package io.lundal.ambisphere.db.model

import io.lundal.ambisphere.util.IdUtils

data class VisualRow(
    val id: String = IdUtils.randomId(),
    val groupId: String,
    val name: String,
    val thumbnailUrl: String,
    val imageUrl: String
)
