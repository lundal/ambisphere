package io.lundal.ambisphere.db.model

import io.lundal.ambisphere.util.IdUtils

data class VisualGroupRow(
    val id: String = IdUtils.randomId(),
    val ambianceId: String,
    val name: String,
    val backgroundColor: String,
    val containImage: Boolean,
    val crossfadeMs: Int
)
