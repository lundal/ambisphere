package io.lundal.ambisphere.db

import io.lundal.ambisphere.db.model.SoundGroupRow
import io.lundal.ambisphere.util.DatabaseUtils.query
import io.lundal.ambisphere.util.DatabaseUtils.queryFirst
import io.lundal.ambisphere.util.DatabaseUtils.update
import java.sql.ResultSet
import javax.enterprise.context.ApplicationScoped
import javax.sql.DataSource

@ApplicationScoped
class SoundGroupDao(private val ds: DataSource) {

    fun insert(row: SoundGroupRow) {
        val sql = """
            insert into sound_groups (id, ambiance_id, name, crossfade_ms, shuffle)
            values (?, ?, ?, ?, ?)
            """
        ds.update(sql, row.id, row.ambianceId, row.name, row.crossfadeMs, row.shuffle)
    }

    fun update(row: SoundGroupRow) {
        val sql = """
            update sound_groups
            set name = ?, crossfade_ms = ?, shuffle = ?
            where id = ?
            """
        ds.update(sql, row.name, row.crossfadeMs, row.shuffle, row.id)
    }

    fun listForAmbiance(ambianceId: String): List<SoundGroupRow> {
        val sql = "select * from sound_groups where ambiance_id = ?"
        return ds.query(sql, ambianceId, rowMapper = ::mapRow)
    }

    fun get(id: String): SoundGroupRow? {
        val sql = "select * from sound_groups where id = ?"
        return ds.queryFirst(sql, id, rowMapper = ::mapRow)
    }

    fun delete(id: String) {
        val sql = "delete from sound_groups where id = ?"
        ds.update(sql, id)
    }

    private fun mapRow(rs: ResultSet) = SoundGroupRow(
        id = rs.getString("id"),
        ambianceId = rs.getString("ambiance_id"),
        name = rs.getString("name"),
        crossfadeMs = rs.getInt("crossfade_ms"),
        shuffle = rs.getBoolean("shuffle")
    )
}
