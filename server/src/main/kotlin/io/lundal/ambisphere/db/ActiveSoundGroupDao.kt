package io.lundal.ambisphere.db

import io.lundal.ambisphere.db.model.ActiveSoundGroupRow
import io.lundal.ambisphere.util.DatabaseUtils.query
import io.lundal.ambisphere.util.DatabaseUtils.update
import java.sql.ResultSet
import javax.enterprise.context.ApplicationScoped
import javax.sql.DataSource

@ApplicationScoped
class ActiveSoundGroupDao(private val ds: DataSource) {

    fun insert(row: ActiveSoundGroupRow) {
        val sql = """
            insert into active_sound_groups (ambiance_id, sound_group_id, created)
            values (?, ?, ?)
            on conflict (ambiance_id, sound_group_id) do update
            set created = excluded.created
            """
        ds.update(sql, row.ambianceId, row.soundGroupId, row.created)
    }

    fun delete(ambianceId: String, visualId: String) {
        val sql = "delete from active_sound_groups where ambiance_id = ? and sound_group_id = ?"
        ds.update(sql, ambianceId, visualId)
    }

    fun listForAmbiance(ambianceId: String): List<ActiveSoundGroupRow> {
        val sql = "select * from active_sound_groups where ambiance_id = ? order by created desc"
        return ds.query(sql, ambianceId, rowMapper = ::mapRow)
    }

    fun deleteForAmbiance(ambianceId: String) {
        val sql = "delete from active_sound_groups where ambiance_id = ?"
        ds.update(sql, ambianceId)
    }

    private fun mapRow(rs: ResultSet) = ActiveSoundGroupRow(
        ambianceId = rs.getString("ambiance_id"),
        soundGroupId = rs.getString("sound_group_id"),
        created = rs.getLong("created")
    )
}
