package io.lundal.ambisphere.db

import io.lundal.ambisphere.db.model.AmbianceRow
import io.lundal.ambisphere.util.DatabaseUtils.query
import io.lundal.ambisphere.util.DatabaseUtils.queryFirst
import io.lundal.ambisphere.util.DatabaseUtils.update
import java.sql.ResultSet
import javax.enterprise.context.ApplicationScoped
import javax.sql.DataSource

@ApplicationScoped
class AmbianceDao(private val ds: DataSource) {

    fun insert(row: AmbianceRow) {
        val sql = """
            insert into ambiances (id, user_id, name, created)
            values (?, ?, ?, ?)
            """
        ds.update(sql, row.id, row.userId, row.name, row.created)
    }

    fun update(row: AmbianceRow) {
        val sql = """
            update ambiances
            set name = ?
            where id = ?
            """
        ds.update(sql, row.name, row.id)
    }

    fun listForUser(userId: String): List<AmbianceRow> {
        val sql = "select * from ambiances where user_id = ?"
        return ds.query(sql, userId, rowMapper = ::mapRow)
    }

    fun get(id: String): AmbianceRow? {
        val sql = "select * from ambiances where id = ?"
        return ds.queryFirst(sql, id, rowMapper = ::mapRow)
    }

    fun delete(id: String) {
        val sql = "delete from ambiances where id = ?"
        ds.update(sql, id)
    }

    fun deleteForUser(userId: String) {
        val sql = "delete from ambiances where user_id = ?"
        ds.update(sql, userId)
    }

    fun uniqueUserIds(): List<String> {
        val sql = "select distinct user_id from ambiances"
        return ds.query(sql) { rs -> rs.getString("user_id") }
    }

    private fun mapRow(rs: ResultSet) = AmbianceRow(
        id = rs.getString("id"),
        userId = rs.getString("user_id"),
        name = rs.getString("name"),
        created = rs.getLong("created")
    )
}
