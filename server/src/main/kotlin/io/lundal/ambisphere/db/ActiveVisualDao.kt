package io.lundal.ambisphere.db

import io.lundal.ambisphere.db.model.ActiveVisualRow
import io.lundal.ambisphere.util.DatabaseUtils.query
import io.lundal.ambisphere.util.DatabaseUtils.update
import java.sql.ResultSet
import javax.enterprise.context.ApplicationScoped
import javax.sql.DataSource

@ApplicationScoped
class ActiveVisualDao(private val ds: DataSource) {

    fun insert(row: ActiveVisualRow) {
        val sql = """
            insert into active_visuals (ambiance_id, visual_id, created)
            values (?, ?, ?)
            on conflict (ambiance_id, visual_id) do update
            set created = excluded.created
            """
        ds.update(sql, row.ambianceId, row.visualId, row.created)
    }

    fun delete(ambianceId: String, visualId: String) {
        val sql = "delete from active_visuals where ambiance_id = ? and visual_id = ?"
        ds.update(sql, ambianceId, visualId)
    }

    fun listForAmbiance(ambianceId: String): List<ActiveVisualRow> {
        val sql = "select * from active_visuals where ambiance_id = ? order by created desc"
        return ds.query(sql, ambianceId, rowMapper = ::mapRow)
    }

    fun deleteForAmbiance(ambianceId: String) {
        val sql = "delete from active_visuals where ambiance_id = ?"
        ds.update(sql, ambianceId)
    }

    private fun mapRow(rs: ResultSet) = ActiveVisualRow(
        ambianceId = rs.getString("ambiance_id"),
        visualId = rs.getString("visual_id"),
        created = rs.getLong("created")
    )
}
