package io.lundal.ambisphere.db

import io.lundal.ambisphere.db.model.SoundRow
import io.lundal.ambisphere.util.DatabaseUtils.query
import io.lundal.ambisphere.util.DatabaseUtils.queryFirst
import io.lundal.ambisphere.util.DatabaseUtils.queryForInt
import io.lundal.ambisphere.util.DatabaseUtils.update
import java.sql.ResultSet
import javax.enterprise.context.ApplicationScoped
import javax.sql.DataSource

@ApplicationScoped
class SoundDao(private val ds: DataSource) {

    fun insert(row: SoundRow) {
        val sql = """
            insert into sounds (id, group_id, name, crop_start_ms, crop_end_ms, length_ms, volume_percent, audio_url)
            values (?, ?, ?, ?, ?, ?, ?, ?)
            """
        ds.update(sql, row.id, row.groupId, row.name, row.cropStartMs, row.cropEndMs, row.lengthMs, row.volumePercent, row.audioUrl)
    }

    fun update(row: SoundRow) {
        val sql = """
            update sounds
            set name = ?, crop_start_ms = ?, crop_end_ms = ?, length_ms = ?, volume_percent = ?, audio_url = ?
            where id = ?
            """
        ds.update(sql, row.name, row.cropStartMs, row.cropEndMs, row.lengthMs, row.volumePercent, row.audioUrl, row.id)
    }

    fun listForAmbiance(ambianceId: String): List<SoundRow> {
        val sql = "select * from sounds where group_id in (select id from sound_groups where ambiance_id = ?)"
        return ds.query(sql, ambianceId, rowMapper = ::mapRow)
    }

    fun countForAmbiance(ambianceId: String): Int {
        val sql = "select count(*) from sounds where group_id in (select id from sound_groups where ambiance_id = ?)"
        return ds.queryForInt(sql, ambianceId)!!
    }

    fun get(id: String): SoundRow? {
        val sql = "select * from sounds where id = ?"
        return ds.queryFirst(sql, id, rowMapper = ::mapRow)
    }

    fun delete(id: String) {
        val sql = "delete from sounds where id = ?"
        ds.update(sql, id)
    }

    private fun mapRow(rs: ResultSet) = SoundRow(
        id = rs.getString("id"),
        groupId = rs.getString("group_id"),
        name = rs.getString("name"),
        cropStartMs = rs.getInt("crop_start_ms"),
        cropEndMs = rs.getInt("crop_end_ms"),
        lengthMs = rs.getInt("length_ms"),
        volumePercent = rs.getInt("volume_percent"),
        audioUrl = rs.getString("audio_url")
    )
}
