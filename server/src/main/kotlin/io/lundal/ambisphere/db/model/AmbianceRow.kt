package io.lundal.ambisphere.db.model

import io.lundal.ambisphere.util.IdUtils
import java.time.Instant

data class AmbianceRow(
    val id: String = IdUtils.randomId(),
    val userId: String,
    val name: String,
    val created: Long = Instant.now().toEpochMilli(),
)
