package io.lundal.ambisphere.db.model

import io.lundal.ambisphere.util.IdUtils
import java.time.Instant

data class FileRow(
    val id: String = IdUtils.randomId(),
    val userId: String,
    val mediatype: String,
    val size: Int,
    val data: ByteArray?,
    val created: Long = Instant.now().toEpochMilli(),
)
