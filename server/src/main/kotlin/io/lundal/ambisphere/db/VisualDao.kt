package io.lundal.ambisphere.db

import io.lundal.ambisphere.db.model.VisualRow
import io.lundal.ambisphere.util.DatabaseUtils.query
import io.lundal.ambisphere.util.DatabaseUtils.queryFirst
import io.lundal.ambisphere.util.DatabaseUtils.queryForInt
import io.lundal.ambisphere.util.DatabaseUtils.update
import java.sql.ResultSet
import javax.enterprise.context.ApplicationScoped
import javax.sql.DataSource

@ApplicationScoped
class VisualDao(private val ds: DataSource) {

    fun insert(row: VisualRow) {
        val sql = """
            insert into visuals (id, group_id, name, thumbnail_url, image_url, position)
            values (?, ?, ?, ?, ?, coalesce((select max(position) + 10 from visuals where group_id = ?), 0))
            """
        ds.update(sql, row.id, row.groupId, row.name, row.thumbnailUrl, row.imageUrl, row.groupId)
    }

    fun update(row: VisualRow) {
        val sql = """
            update visuals
            set name = ?, thumbnail_url = ?, image_url = ?
            where id = ?
            """
        ds.update(sql, row.name, row.thumbnailUrl, row.imageUrl, row.id)
    }

    fun updateGroupAndPosition(id: String, groupId: String, position: Int) {
        val sql = """
            update visuals
            set group_id = ?, position = ?
            where id = ?
            """
        ds.update(sql, groupId, position, id)
    }

    fun rebalancePositions(groupId: String) {
        val sql = """
            update visuals v1
            set position = v2.rownum
            from (
                select *, 10 * row_number() over (order by v2.position, v2.name) as rownum
                from visuals v2
                where v2.group_id = ?
            ) as v2
            where v1.id = v2.id
            """
        ds.update(sql, groupId)
    }

    fun listForAmbiance(ambianceId: String): List<VisualRow> {
        val sql = "select * from visuals where group_id in (select id from visual_groups where ambiance_id = ?) order by position, name"
        return ds.query(sql, ambianceId, rowMapper = ::mapRow)
    }

    fun countForAmbiance(ambianceId: String): Int {
        val sql = "select count(*) from visuals where group_id in (select id from visual_groups where ambiance_id = ?)"
        return ds.queryForInt(sql, ambianceId)!!
    }

    fun get(id: String): VisualRow? {
        val sql = "select * from visuals where id = ?"
        return ds.queryFirst(sql, id, rowMapper = ::mapRow)
    }

    fun delete(id: String) {
        val sql = "delete from visuals where id = ?"
        ds.update(sql, id)
    }

    private fun mapRow(rs: ResultSet) = VisualRow(
        id = rs.getString("id"),
        groupId = rs.getString("group_id"),
        name = rs.getString("name"),
        thumbnailUrl = rs.getString("thumbnail_url"),
        imageUrl = rs.getString("image_url")
    )
}
