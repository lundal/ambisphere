package io.lundal.ambisphere.db.model

import java.time.Instant

data class ActiveVisualRow(
    val ambianceId: String,
    val visualId: String,
    val created: Long = Instant.now().toEpochMilli(),
)
