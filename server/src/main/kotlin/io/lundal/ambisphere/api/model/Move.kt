package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty

@RegisterForReflection
data class Move(
    @get:NotEmpty
    val groupId: String,

    @get:Min(0)
    val position: Int,
)
