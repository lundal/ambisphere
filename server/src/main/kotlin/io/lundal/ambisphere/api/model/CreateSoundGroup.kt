package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

@RegisterForReflection
data class CreateSoundGroup(
    @get:NotEmpty
    @get:Size(max = 64)
    val name: String,

    @get:Min(0)
    @get:Max(10000)
    val crossfadeMs: Int,

    val shuffle: Boolean
)
