package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class VisualGroup(
    val id: String,
    val name: String,
    val backgroundColor: String,
    val containImage: Boolean,
    val crossfadeMs: Int,
    val visuals: List<Visual>
)
