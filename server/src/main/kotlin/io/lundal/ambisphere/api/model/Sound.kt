package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class Sound(
    val id: String,
    val name: String,
    val cropStartMs: Int,
    val cropEndMs: Int,
    val lengthMs: Int,
    val volumePercent: Int,
    val audioUrl: String
)
