package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

@RegisterForReflection
data class CreateVisualGroup(
    @get:NotEmpty
    @get:Size(max = 64)
    val name: String,

    @get:NotEmpty
    @get:Size(max = 64)
    val backgroundColor: String,

    val containImage: Boolean,

    @get:Min(0)
    @get:Max(10000)
    val crossfadeMs: Int
)
