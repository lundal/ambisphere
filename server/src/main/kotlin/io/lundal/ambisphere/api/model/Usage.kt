package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class Usage(
    val current: Int,
    val limit: Int,
)
