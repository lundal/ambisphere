package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class Error(
    val code: Int,
    val reason: String,
    val details: String?
)
