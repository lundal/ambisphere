package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

@RegisterForReflection
data class CreateAmbiance(
    @get:NotEmpty
    @get:Size(max = 64)
    val name: String,

    // Jackson fails if there is only one property
    val dummy: String? = null,
)


