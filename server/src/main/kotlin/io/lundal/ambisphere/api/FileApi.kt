package io.lundal.ambisphere.api

import io.lundal.ambisphere.api.model.CreateFile
import io.lundal.ambisphere.api.model.File
import io.lundal.ambisphere.api.model.Usage
import io.lundal.ambisphere.auth.AuthenticationService
import io.lundal.ambisphere.db.FileDao
import io.lundal.ambisphere.db.model.FileRow
import io.lundal.ambisphere.exceptions.ServiceExceptions
import io.lundal.ambisphere.s3.S3Service
import io.lundal.mediaconverter.api.AudioApi
import io.lundal.mediaconverter.api.ImageApi
import io.lundal.mediaconverter.api.model.*
import org.eclipse.microprofile.config.inject.ConfigProperty
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/api/v1/files")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
class FileApi(
    private val fileDao: FileDao,
    private val s3Service: S3Service,
    private val authenticationService: AuthenticationService,
    @RestClient private val audioApi: AudioApi,
    @RestClient private val imageApi: ImageApi,
    @ConfigProperty(name = "file.storage-per-user") private val storagePerUser: Int,
    @ConfigProperty(name = "file.upload-to.s3") private val uploadToS3: Boolean,
    @ConfigProperty(name = "file.url-prefix.db") private val urlPrefixDB: String,
    @ConfigProperty(name = "file.url-prefix.s3") private val urlPrefixS3: String,
) {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)
    val audioSpec = AudioSpec(format = AudioFormat.OPUS, bitrate = 64000)
    val imageSpec = ImageSpec(format = ImageFormat.WEBP, size = ImageSize(2048, 2048), quality = 60)
    val thumbnailSpec = ImageSpec(format = ImageFormat.WEBP, size = ImageSize(512, 512), quality = 40)

    @GET
    @Path("/{id}")
    @Produces(MediaType.WILDCARD)
    fun getFile(@PathParam("id") id: String): Response {
        val fileRow = fileDao.get(id) ?: throw ServiceExceptions.fileNotFound()
        return Response.ok(fileRow.data, fileRow.mediatype).build()
    }

    @GET
    @Path("/usage")
    fun getUsage(): Usage {
        val user = authenticationService.requireUser()
        return Usage(
            current = fileDao.getUsage(user.id),
            limit = storagePerUser
        )
    }

    @POST
    fun uploadFile(@QueryParam("compress") compress: String?, createFile: CreateFile): File {
        val user = authenticationService.requireUser()

        // Backend compression can be removed when there is
        // good image and audio compression in the clients
        val compressedFile = compressFile(compress, createFile)

        if (fileDao.getUsage(user.id) + compressedFile.data.size > storagePerUser) {
            throw ServiceExceptions.notEnoughStorage()
        }

        if (uploadToS3) {
            val fileRow = FileRow(
                userId = user.id,
                mediatype = compressedFile.mediatype,
                size = compressedFile.data.size,
                data = null
            )
            s3Service.uploadFile(fileRow.id, compressedFile.mediatype, compressedFile.data)
            fileDao.insert(fileRow)
            return File(
                mediatype = compressedFile.mediatype,
                url = "$urlPrefixS3/${fileRow.id}"
            )
        } else {
            val fileRow = FileRow(
                userId = user.id,
                mediatype = compressedFile.mediatype,
                size = compressedFile.data.size,
                data = compressedFile.data
            )
            fileDao.insert(fileRow)
            return File(
                mediatype = compressedFile.mediatype,
                url = "$urlPrefixDB/${fileRow.id}"
            )
        }
    }

    private fun compressFile(compress: String?, createFile: CreateFile): CreateFile {
        return when (compress) {
            null -> createFile
            "audio" -> convertAudio(createFile.toAudio(), audioSpec).toFile()
            "image" -> convertImage(createFile.toImage(), imageSpec).toFile()
            "thumbnail" -> convertImage(createFile.toImage(), thumbnailSpec).toFile()
            else -> throw ServiceExceptions.unsupportedCompressionType()
        }
    }

    private fun convertAudio(image: Audio, spec: AudioSpec): Audio {
        try {
            return audioApi.convertAudio(ConvertAudioRequest(image, listOf(spec)))[0]
        } catch (e: Exception) {
            logger.error("Failed to convert audio", e)
            throw ServiceExceptions.audioConversionFailed()
        }
    }

    private fun convertImage(image: Image, spec: ImageSpec): Image {
        try {
            return imageApi.convertImage(ConvertImageRequest(image, listOf(spec)))[0]
        } catch (e: Exception) {
            logger.error("Failed to convert image", e)
            throw ServiceExceptions.imageConversionFailed()
        }
    }

    private fun CreateFile.toImage(): Image {
        return Image(this.mediatype.toImageFormat(), this.data)
    }

    private fun Image.toFile(): CreateFile {
        return CreateFile(this.format.toMediatype(), this.data)
    }

    private fun CreateFile.toAudio(): Audio {
        return Audio(this.mediatype.toAudioFormat(), this.data)
    }

    private fun Audio.toFile(): CreateFile {
        return CreateFile(this.format.toMediatype(), this.data)
    }

    private fun String.toAudioFormat(): AudioFormat {
        return AudioFormat.values().firstOrNull { it.type == this }
            ?: throw ServiceExceptions.audioConversionFailed()
    }

    private fun AudioFormat.toMediatype(): String {
        return this.type
    }

    private fun String.toImageFormat(): ImageFormat {
        return ImageFormat.values().firstOrNull { it.type == this }
            ?: throw ServiceExceptions.imageConversionFailed()
    }

    private fun ImageFormat.toMediatype(): String {
        return this.type
    }

}
