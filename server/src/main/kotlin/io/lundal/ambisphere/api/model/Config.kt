package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class Config(
    val user: User?,
    val accountUrl: String,
    val appLinks: List<AppLink>,
)
