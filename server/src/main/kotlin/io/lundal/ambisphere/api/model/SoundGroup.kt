package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class SoundGroup(
    val id: String,
    val name: String,
    val crossfadeMs: Int,
    val shuffle: Boolean,
    val sounds: List<Sound>
)
