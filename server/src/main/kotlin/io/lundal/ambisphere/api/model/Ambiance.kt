package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class Ambiance(
    val id: String,
    val name: String,
    val soundGroups: List<SoundGroup>,
    val visualGroups: List<VisualGroup>
)
