package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class Actives(
    val soundGroups: List<Active>,
    val visual: Active?,
)
