package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class AmbianceSummary(
    val id: String,
    val name: String,
    val sounds: Int,
    val visuals: Int,
)
