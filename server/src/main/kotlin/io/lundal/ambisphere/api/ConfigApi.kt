package io.lundal.ambisphere.api

import io.lundal.ambisphere.api.model.AppLink
import io.lundal.ambisphere.api.model.Config
import io.lundal.ambisphere.api.model.User
import io.lundal.ambisphere.auth.AuthenticationService
import io.lundal.ambisphere.exceptions.ServiceException
import org.eclipse.microprofile.config.inject.ConfigProperty
import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType.APPLICATION_JSON

@Path("/api/v1/config")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
class ConfigApi(
    private val authenticationService: AuthenticationService,
    @ConfigProperty(name = "quarkus.rest-client.account.url") private val accountUrl: String,
    @ConfigProperty(name = "app.links") private val applications: List<String>,
) {

    @GET
    fun getConfig(): Config {
        return Config(
            user = getUser(),
            accountUrl = accountUrl,
            appLinks = applications.map {
                val (name, iconUrl, linkUrl) = it.split("|")
                AppLink(name, iconUrl, linkUrl)
            }
        )
    }

    private fun getUser(): User? =
        try {
            val user = authenticationService.requireUser()
            User(id = user.id, name = user.name)
        } catch (e: ServiceException) {
            null
        }

}
