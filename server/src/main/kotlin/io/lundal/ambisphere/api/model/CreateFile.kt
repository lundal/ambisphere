package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Pattern

@RegisterForReflection
data class CreateFile(
    @get:NotEmpty
    @get:Pattern(regexp = "(image|audio)/(.+)")
    val mediatype: String,

    val data: ByteArray,
)
