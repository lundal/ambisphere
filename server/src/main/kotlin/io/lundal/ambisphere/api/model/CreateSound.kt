package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

@RegisterForReflection
data class CreateSound(
    @get:NotEmpty
    @get:Size(max = 64)
    val name: String,

    @get:Min(0)
    val cropStartMs: Int,

    @get:Min(0)
    val cropEndMs: Int,

    @get:Min(0)
    val lengthMs: Int,

    @get:Min(0)
    @get:Max(100)
    val volumePercent: Int,

    @get:Size(max = 2048)
    val audioUrl: String
)
