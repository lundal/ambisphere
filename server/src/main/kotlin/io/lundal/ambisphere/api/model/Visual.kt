package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class Visual(
    val id: String,
    val name: String,
    val thumbnailUrl: String,
    val imageUrl: String
)
