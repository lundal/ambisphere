package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.Size

@RegisterForReflection
data class CreateVisual(
    @get:NotEmpty
    @get:Size(max = 64)
    val name: String,

    @get:Size(max = 2048)
    val thumbnailUrl: String,

    @get:Size(max = 2048)
    val imageUrl: String
)
