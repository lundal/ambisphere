package io.lundal.ambisphere.api

import io.lundal.ambisphere.api.model.*
import io.lundal.ambisphere.auth.AuthenticationService
import io.lundal.ambisphere.db.*
import io.lundal.ambisphere.db.model.*
import io.lundal.ambisphere.exceptions.ServiceExceptions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.validation.Valid
import javax.ws.rs.*
import javax.ws.rs.core.MediaType.APPLICATION_JSON

@Path("/api/v1/ambiances")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
class AmbianceApi(
    private val ambianceDao: AmbianceDao,
    private val soundGroupDao: SoundGroupDao,
    private val soundDao: SoundDao,
    private val visualGroupDao: VisualGroupDao,
    private val visualDao: VisualDao,
    private val activeSoundGroupDao: ActiveSoundGroupDao,
    private val activeVisualDao: ActiveVisualDao,
    private val authenticationService: AuthenticationService
) {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @POST
    fun createAmbiance(@Valid create: CreateAmbiance): Ambiance {
        val user = authenticationService.requireUser()

        val row = AmbianceRow(
            userId = user.id,
            name = create.name
        )
        ambianceDao.insert(row)

        logger.info("Created ambiance '${row.id}' for user ${row.userId}")

        return mapAmbiance(row)
    }

    @PUT
    @Path("/{ambianceId}")
    fun updateAmbiance(
        @PathParam("ambianceId") ambianceId: String,
        @Valid update: CreateAmbiance
    ): Ambiance {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        val row = ambianceRow.copy(
            name = update.name,
        )
        ambianceDao.update(row)

        logger.info("Updated ambiance '${row.id}'")

        return mapAmbiance(row)
    }

    @POST
    @Path("/{ambianceId}/sound-groups")
    fun createSoundGroup(
        @PathParam("ambianceId") ambianceId: String,
        @Valid create: CreateSoundGroup
    ): SoundGroup {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        val row = SoundGroupRow(
            ambianceId = ambianceId,
            name = create.name,
            crossfadeMs = create.crossfadeMs,
            shuffle = create.shuffle
        )
        soundGroupDao.insert(row)

        logger.info("Created sound group '${row.id}' for ambiance ${row.ambianceId}")

        return mapSoundGroup(row, listOf())
    }

    @PUT
    @Path("/{ambianceId}/sound-groups/{groupId}")
    fun updateSoundGroup(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String,
        @Valid update: CreateSoundGroup
    ): SoundGroup {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        val soundGroupRow = soundGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.soundGroupNotFound()

        val row = soundGroupRow.copy(
            name = update.name,
            crossfadeMs = update.crossfadeMs,
            shuffle = update.shuffle
        )
        soundGroupDao.update(row)

        logger.info("Updated sound group '${row.id}' for ambiance ${row.ambianceId}")

        return mapSoundGroup(row, listOf())
    }

    @DELETE
    @Path("/{ambianceId}/sound-groups/{groupId}")
    fun deleteSoundGroup(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String
    ) {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        soundGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: return

        soundGroupDao.delete(groupId)
    }

    @POST
    @Path("/{ambianceId}/sound-groups/{groupId}/activate")
    fun activateSoundGroup(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String,
    ) {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        soundGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.soundGroupNotFound()

        val row = ActiveSoundGroupRow(
            ambianceId = ambianceId,
            soundGroupId = groupId,
        )

        activeSoundGroupDao.insert(row)
    }

    @POST
    @Path("/{ambianceId}/sound-groups/{groupId}/deactivate")
    fun deactivateSoundGroup(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String,
    ) {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        soundGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.soundGroupNotFound()

        activeSoundGroupDao.delete(ambianceId, groupId)
    }

    @POST
    @Path("/{ambianceId}/sound-groups/{groupId}")
    fun createSound(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String,
        @Valid create: CreateSound
    ): Sound {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        soundGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.soundGroupNotFound()

        val row = SoundRow(
            groupId = groupId,
            name = create.name,
            cropStartMs = create.cropStartMs,
            cropEndMs = create.cropEndMs,
            lengthMs = create.lengthMs,
            volumePercent = create.volumePercent,
            audioUrl = create.audioUrl
        )
        soundDao.insert(row)

        logger.info("Created sound '${row.id}' for sound group ${row.groupId}")

        return mapSound(row)
    }

    @PUT
    @Path("/{ambianceId}/sound-groups/{groupId}/sounds/{soundId}")
    fun updateSound(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String,
        @PathParam("soundId") soundId: String,
        @Valid update: CreateSound
    ): Sound {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        soundGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.soundGroupNotFound()

        val soundRow = soundDao.get(soundId)
            ?.takeIf { it.groupId == groupId }
            ?: throw ServiceExceptions.soundNotFound()

        val row = soundRow.copy(
            name = update.name,
            cropStartMs = update.cropStartMs,
            cropEndMs = update.cropEndMs,
            lengthMs = update.lengthMs,
            volumePercent = update.volumePercent,
            audioUrl = update.audioUrl
        )
        soundDao.update(row)

        logger.info("Updated sound '${row.id}' for sound group ${row.groupId}")

        return mapSound(row)
    }

    @DELETE
    @Path("/{ambianceId}/sound-groups/{groupId}/sounds/{soundId}")
    fun deleteSound(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String,
        @PathParam("soundId") soundId: String
    ) {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        soundGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.soundGroupNotFound()

        soundDao.get(soundId)
            ?.takeIf { it.groupId == groupId }
            ?: return

        soundDao.delete(soundId)
    }

    // TODO: Move sound?

    @POST
    @Path("/{ambianceId}/visual-groups")
    fun createVisualGroup(
        @PathParam("ambianceId") ambianceId: String,
        @Valid create: CreateVisualGroup
    ): VisualGroup {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        val row = VisualGroupRow(
            ambianceId = ambianceId,
            name = create.name,
            backgroundColor = create.backgroundColor,
            containImage = create.containImage,
            crossfadeMs = create.crossfadeMs
        )
        visualGroupDao.insert(row)

        logger.info("Created visual group '${row.id}' for ambiance ${row.ambianceId}")

        return mapVisualGroup(row, listOf())
    }

    @PUT
    @Path("/{ambianceId}/visual-groups/{groupId}")
    fun updateVisualGroup(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String,
        @Valid update: CreateVisualGroup
    ): VisualGroup {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        val visualGroupRow = visualGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.visualGroupNotFound()

        val row = visualGroupRow.copy(
            name = update.name,
            backgroundColor = update.backgroundColor,
            containImage = update.containImage,
            crossfadeMs = update.crossfadeMs
        )
        visualGroupDao.update(row)

        logger.info("Updated visual group '${row.id}' for ambiance ${row.ambianceId}")

        return mapVisualGroup(row, listOf())
    }

    @DELETE
    @Path("/{ambianceId}/visual-groups/{groupId}")
    fun deleteVisualGroup(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String
    ) {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        visualGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: return

        visualGroupDao.delete(groupId)
    }

    @POST
    @Path("/{ambianceId}/visual-groups/{groupId}")
    fun createVisual(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String,
        @Valid create: CreateVisual
    ): Visual {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        visualGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.visualGroupNotFound()

        val row = VisualRow(
            groupId = groupId,
            name = create.name,
            thumbnailUrl = create.thumbnailUrl,
            imageUrl = create.imageUrl,
        )
        visualDao.insert(row)

        logger.info("Created visual '${row.id}' for visual group ${row.groupId}")

        return mapVisual(row)
    }

    @PUT
    @Path("/{ambianceId}/visual-groups/{groupId}/visuals/{visualId}")
    fun updateVisual(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String,
        @PathParam("visualId") visualId: String,
        @Valid update: CreateVisual
    ): Visual {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        visualGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.visualGroupNotFound()

        val visualRow = visualDao.get(visualId)
            ?.takeIf { it.groupId == groupId }
            ?: throw ServiceExceptions.visualNotFound()

        val row = visualRow.copy(
            name = update.name,
            thumbnailUrl = update.thumbnailUrl,
            imageUrl = update.imageUrl,
        )
        visualDao.update(row)

        logger.info("Updated visual '${row.id}' for visual group ${row.groupId}")

        return mapVisual(row)
    }

    @DELETE
    @Path("/{ambianceId}/visual-groups/{groupId}/visuals/{visualId}")
    fun deleteVisual(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String,
        @PathParam("visualId") visualId: String
    ) {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        visualGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.visualGroupNotFound()

        visualDao.get(visualId)
            ?.takeIf { it.groupId == groupId }
            ?: return

        visualDao.delete(visualId)
        visualDao.rebalancePositions(groupId)
    }

    @POST
    @Path("/{ambianceId}/visual-groups/{groupId}/visuals/{visualId}/move")
    fun moveVisual(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String,
        @PathParam("visualId") visualId: String,
        @Valid move: Move
    ) {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        visualGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.visualGroupNotFound()

        visualDao.get(visualId)
            ?.takeIf { it.groupId == groupId }
            ?: throw ServiceExceptions.visualNotFound()

        visualGroupDao.get(move.groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.invalidTargetGroup()

        visualDao.updateGroupAndPosition(visualId, move.groupId, move.position * 10 + 5)
        visualDao.rebalancePositions(groupId)
        visualDao.rebalancePositions(move.groupId)
    }

    @POST
    @Path("/{ambianceId}/visual-groups/{groupId}/visuals/{visualId}/activate")
    fun activateVisual(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String,
        @PathParam("visualId") visualId: String
    ) {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        visualGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.visualGroupNotFound()

        visualDao.get(visualId)
            ?.takeIf { it.groupId == groupId }
            ?: throw ServiceExceptions.visualNotFound()

        val row = ActiveVisualRow(
            ambianceId = ambianceId,
            visualId = visualId
        )

        // TODO: Support multiple?
        activeVisualDao.deleteForAmbiance(ambianceId)
        activeVisualDao.insert(row)
    }

    @POST
    @Path("/{ambianceId}/visual-groups/{groupId}/visuals/{visualId}/deactivate")
    fun deactivateVisual(
        @PathParam("ambianceId") ambianceId: String,
        @PathParam("groupId") groupId: String,
        @PathParam("visualId") visualId: String
    ) {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        visualGroupDao.get(groupId)
            ?.takeIf { it.ambianceId == ambianceId }
            ?: throw ServiceExceptions.visualGroupNotFound()

        visualDao.get(visualId)
            ?.takeIf { it.groupId == groupId }
            ?: throw ServiceExceptions.visualNotFound()

        activeVisualDao.delete(ambianceId, visualId)
    }

    @GET
    fun listAmbiances(): List<AmbianceSummary> {
        val user = authenticationService.requireUser()

        return ambianceDao.listForUser(user.id).map { row ->
            AmbianceSummary(
                id = row.id,
                name = row.name,
                sounds = soundDao.countForAmbiance(row.id),
                visuals = visualDao.countForAmbiance(row.id)
            )
        }
    }

    @GET
    @Path("/{ambianceId}")
    fun getAmbiance(@PathParam("ambianceId") ambianceId: String): Ambiance {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()

        val soundGroupRows = soundGroupDao.listForAmbiance(ambianceId)
        val soundRows = soundDao.listForAmbiance(ambianceId)
        val visualGroupRows = visualGroupDao.listForAmbiance(ambianceId)
        val visualRows = visualDao.listForAmbiance(ambianceId)

        return mapAmbiance(ambianceRow, soundGroupRows, soundRows, visualGroupRows, visualRows)
    }

    @DELETE
    @Path("/{ambianceId}")
    fun deleteAmbiance(@PathParam("ambianceId") ambianceId: String) {
        val ambianceRow = ambianceDao.get(ambianceId) ?: return
        authenticationService.requireUser { it.id == ambianceRow.userId }

        ambianceDao.delete(ambianceId)

        logger.info("Deleted ambiance with id '${ambianceId}'")
    }

    @GET
    @Path("/{ambianceId}/actives")
    fun getActives(@PathParam("ambianceId") ambianceId: String): Actives {
        val activeSoundGroups = activeSoundGroupDao.listForAmbiance(ambianceId)
        val activeVisuals = activeVisualDao.listForAmbiance(ambianceId)

        return Actives(
            soundGroups = activeSoundGroups
                .map { Active(id = it.soundGroupId, startedMs = it.created) },
            visual = activeVisuals
                .map { Active(id = it.visualId, startedMs = it.created) }
                .firstOrNull(),
        )
    }

    @DELETE
    @Path("/{ambianceId}/actives")
    fun deleteActives(@PathParam("ambianceId") ambianceId: String) {
        val ambianceRow = ambianceDao.get(ambianceId) ?: throw ServiceExceptions.ambianceNotFound()
        authenticationService.requireUser { it.id == ambianceRow.userId }

        activeSoundGroupDao.deleteForAmbiance(ambianceId)
        activeVisualDao.deleteForAmbiance(ambianceId)
    }

    private fun mapAmbiance(row: AmbianceRow): Ambiance = Ambiance(
        id = row.id,
        name = row.name,
        visualGroups = listOf(),
        soundGroups = listOf()
    )

    private fun mapAmbiance(
        ambianceRow: AmbianceRow,
        soundGroupRows: List<SoundGroupRow>,
        soundRows: List<SoundRow>,
        visualGroupRows: List<VisualGroupRow>,
        visualRows: List<VisualRow>
    ) = Ambiance(
        id = ambianceRow.id,
        name = ambianceRow.name,
        soundGroups = soundGroupRows.map { group -> mapSoundGroup(group, soundRows) },
        visualGroups = visualGroupRows.map { group -> mapVisualGroup(group, visualRows) }
    )

    private fun mapSoundGroup(row: SoundGroupRow, soundRows: List<SoundRow>) = SoundGroup(
        id = row.id,
        name = row.name,
        crossfadeMs = row.crossfadeMs,
        shuffle = row.shuffle,
        sounds = soundRows.filter { it.groupId == row.id }.map(this::mapSound)
    )

    private fun mapSound(row: SoundRow) = Sound(
        id = row.id,
        name = row.name,
        cropStartMs = row.cropStartMs,
        cropEndMs = row.cropEndMs,
        lengthMs = row.lengthMs,
        volumePercent = row.volumePercent,
        audioUrl = row.audioUrl
    )

    private fun mapVisualGroup(row: VisualGroupRow, visualRows: List<VisualRow>) = VisualGroup(
        id = row.id,
        name = row.name,
        backgroundColor = row.backgroundColor,
        containImage = row.containImage,
        crossfadeMs = row.crossfadeMs,
        visuals = visualRows.filter { it.groupId == row.id }.map(this::mapVisual)
    )

    private fun mapVisual(row: VisualRow) = Visual(
        id = row.id,
        name = row.name,
        thumbnailUrl = row.thumbnailUrl,
        imageUrl = row.imageUrl
    )

}
