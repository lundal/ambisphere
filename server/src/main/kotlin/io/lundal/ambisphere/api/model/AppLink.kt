package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class AppLink(
    val name: String,
    val iconUrl: String,
    val linkUrl: String,
)
