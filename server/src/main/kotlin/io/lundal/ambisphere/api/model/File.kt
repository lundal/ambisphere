package io.lundal.ambisphere.api.model

import io.quarkus.runtime.annotations.RegisterForReflection

@RegisterForReflection
data class File(
    val mediatype: String,
    val url: String,
)
