package io.lundal.ambisphere.exceptions.mappers

import io.lundal.ambisphere.exceptions.ServiceExceptions
import javax.validation.ConstraintViolationException
import javax.validation.Path
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class ConstraintViolationExceptionMapper : ExceptionMapper<ConstraintViolationException> {

    override fun toResponse(e: ConstraintViolationException): Response {
        val violations = e.constraintViolations.joinToString { violation ->
            "${formatPath(violation.propertyPath)} ${violation.message}"
        }
        return ServiceExceptionMapper().toResponse(ServiceExceptions.validationFailed(violations))
    }

    private fun formatPath(path: Path): String {
        // The first two nodes are <method name> and <parameter name>
        val relativeToBody = path.drop(2).joinToString(".")

        // Special case for @NotNull on parameter
        if (relativeToBody.isBlank()) {
            return "body"
        }

        return relativeToBody
    }

}
