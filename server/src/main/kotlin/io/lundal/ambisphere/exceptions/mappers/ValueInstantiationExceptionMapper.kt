package io.lundal.ambisphere.exceptions.mappers

import com.fasterxml.jackson.databind.exc.ValueInstantiationException
import io.lundal.ambisphere.exceptions.ServiceExceptions
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class ValueInstantiationExceptionMapper : ExceptionMapper<ValueInstantiationException> {

    override fun toResponse(e: ValueInstantiationException): Response {
        if (e.cause is NullPointerException) {
            val parameter = e.cause?.message?.split(" ")?.lastOrNull() ?: "<unknown>"
            return ServiceExceptionMapper().toResponse(ServiceExceptions.validationFailed("$parameter was null"))
        }
        return ServiceExceptionMapper().toResponse(ServiceExceptions.unexpectedError(e))
    }

}
