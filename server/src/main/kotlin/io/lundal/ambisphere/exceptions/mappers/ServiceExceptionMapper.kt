package io.lundal.ambisphere.exceptions.mappers

import io.lundal.ambisphere.api.model.Error
import io.lundal.ambisphere.exceptions.ServiceException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class ServiceExceptionMapper : ExceptionMapper<ServiceException> {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    override fun toResponse(e: ServiceException): Response {
        if (e.code in 400..499) {
            logger.warn(e.message)
        } else {
            logger.error(e.message, e)
        }
        return Response.status(e.code).entity(Error(e.code, e.reason, e.details)).build()
    }

}
