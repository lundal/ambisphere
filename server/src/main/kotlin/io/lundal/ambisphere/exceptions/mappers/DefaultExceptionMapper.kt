package io.lundal.ambisphere.exceptions.mappers

import io.lundal.ambisphere.exceptions.ServiceExceptions
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class DefaultExceptionMapper : ExceptionMapper<Exception> {

    override fun toResponse(e: Exception): Response {
        return ServiceExceptionMapper().toResponse(ServiceExceptions.unexpectedError(e))
    }

}
