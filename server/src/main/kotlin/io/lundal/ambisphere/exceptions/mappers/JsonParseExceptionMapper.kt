package io.lundal.ambisphere.exceptions.mappers

import com.fasterxml.jackson.core.JsonParseException
import io.lundal.ambisphere.exceptions.ServiceExceptions
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class JsonParseExceptionMapper : ExceptionMapper<JsonParseException> {

    override fun toResponse(e: JsonParseException): Response {
        val details = "${e.originalMessage} [Line ${e.location.lineNr}, Column ${e.location.columnNr}]"
        return ServiceExceptionMapper().toResponse(ServiceExceptions.invalidJson(details))
    }

}
