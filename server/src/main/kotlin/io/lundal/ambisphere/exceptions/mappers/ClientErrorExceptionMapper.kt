package io.lundal.ambisphere.exceptions.mappers

import javax.ws.rs.ClientErrorException
import javax.ws.rs.core.Response
import javax.ws.rs.ext.ExceptionMapper
import javax.ws.rs.ext.Provider

@Provider
class ClientErrorExceptionMapper : ExceptionMapper<ClientErrorException> {

    override fun toResponse(e: ClientErrorException): Response {
        return e.response
    }

}
