package io.lundal.ambisphere.exceptions

object ServiceExceptions {

    // Auth
    fun sessionHeaderMissing() = ServiceException(401, "Not authenticated", "Missing session header")
    fun sessionHeaderInvalid() = ServiceException(401, "Not authenticated", "Invalid session header")
    fun sessionCookieMissing() = ServiceException(401, "Not authenticated", "Missing session cookie")
    fun sessionCookieInvalid() = ServiceException(401, "Not authenticated", "Invalid session cookie")
    fun permissionDenied() = ServiceException(403, "Permission denied")

    // General
    fun invalidJson(details: String) = ServiceException(400, "Invalid JSON", details)
    fun validationFailed(violations: String) = ServiceException(400, "Validation failed", violations)
    fun resourceNotFound() = ServiceException(404, "Resource not found")
    fun unexpectedError(e: Exception) = ServiceException(500, "Unexpected error", null, e)

    // Specific
    fun ambianceNotFound() = ServiceException(404, "Ambiance not found")
    fun soundGroupNotFound() = ServiceException(404, "Sound group not found")
    fun soundNotFound() = ServiceException(404, "Sound not found")
    fun visualGroupNotFound() = ServiceException(404, "Visual group not found")
    fun visualNotFound() = ServiceException(404, "Visual not found")
    fun fileNotFound() = ServiceException(404, "File not found")
    fun audioConversionFailed() = ServiceException(500, "Audio conversion failed")
    fun imageConversionFailed() = ServiceException(500, "Image conversion failed")
    fun unsupportedCompressionType() = ServiceException(400, "Unsupported compression type")
    fun notEnoughStorage() = ServiceException(400, "Not enough remaining storage space")
    fun invalidTargetGroup() = ServiceException(400, "Invalid target group")

}
