package io.lundal.ambisphere.exceptions

data class ServiceException(
    val code: Int,
    val reason: String,
    val details: String? = null,
    override val cause: Throwable? = null
) : RuntimeException("$code $reason - $details", cause)
