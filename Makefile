.PHONY: push image binary test

image_name = registry.gitlab.com/lundal/ambisphere
image_version = $(shell git branch --show-current)

server_binary = server/build/application
web_index = web/dist/index.html

# Push docker image to registry
push: image
	docker push $(image_name):$(image_version)

# Build docker image
image: $(server_binary) Dockerfile
	docker build --tag $(image_name):$(image_version) .

# Build binary
binary: $(server_binary)

# Build server binary
$(server_binary): $(shell find server/src -type f) $(shell find server -maxdepth 1 -type f) $(web_index)
	cd server && ./gradlew clean
	cd server && ./gradlew build --info
	mv server/build/*-runner $(server_binary)

# Build web resources
$(web_index): $(shell find web/src -type f) $(shell find web -maxdepth 1 -type f)
	cd web && yarn install
	cd web && yarn clean
	cd web && yarn build

# Run integration tests
test:
	cd test && yarn install
	cd test && yarn test
